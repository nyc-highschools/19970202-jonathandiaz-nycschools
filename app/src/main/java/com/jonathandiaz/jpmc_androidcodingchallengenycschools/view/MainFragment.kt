package com.jonathandiaz.jpmc_androidcodingchallengenycschools.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.databinding.FragmentMainBinding
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.model.Highschool
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.api.*
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.viewmodel.HighSchoolRecyclerAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/*
 *      MainFragment
 *      - shows the UI
 *      - listens to viewModel for updates on UI
 */
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    // View Binding
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private lateinit var highSchoolRecyclerAdapter: HighSchoolRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // This is needed for view binding
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        highSchoolRecyclerAdapter = context?.let { HighSchoolRecyclerAdapter(it, requireActivity()) }!!
        binding.recyclerview.layoutManager = LinearLayoutManager(context)
        binding.recyclerview.adapter = highSchoolRecyclerAdapter

        highSchoolNames()

    }

    private fun highSchoolNames() {
        val apiInterface = ApiInterface.create().getHighSchoolNames()

        apiInterface.enqueue(object : Callback<List<Highschool>> {
            override fun onResponse(
                call: Call<List<Highschool>>,
                response: Response<List<Highschool>>
            ) {
                if (response.body() != null)
                    highSchoolRecyclerAdapter.setHighSchoolNamesListItems(response.body()!!)
            }

            override fun onFailure(call: Call<List<Highschool>>, t: Throwable) {
                Log.d("wrong", " did not go through")
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}