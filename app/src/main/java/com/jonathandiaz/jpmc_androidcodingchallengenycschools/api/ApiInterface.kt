package com.jonathandiaz.jpmc_androidcodingchallengenycschools.api

import com.jonathandiaz.jpmc_androidcodingchallengenycschools.model.AverageSatScores
import com.jonathandiaz.jpmc_androidcodingchallengenycschools.model.Highschool
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {
    @GET("s3k6-pzi2.json")
    fun getHighSchoolNames(): Call<List<Highschool>>

    @GET("f9bf-2cp4.json")
    fun getSAT(): Call<List<AverageSatScores>>

    companion object {

        var BASE_URL = "https://data.cityofnewyork.us/resource/"

        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }
}