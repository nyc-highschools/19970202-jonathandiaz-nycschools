package com.jonathandiaz.jpmc_androidcodingchallengenycschools.model

import com.google.gson.annotations.SerializedName

data class Highschool(
    @SerializedName("dbn")
    var id: String,
    @SerializedName("school_name")
    var name: String
)

data class AverageSatScores(
    @SerializedName("dbn")
    val id: String,
    @SerializedName("sat_math_avg_score")
    val math: String,
    @SerializedName("sat_critical_reading_avg_score")
    val reading: String,
    @SerializedName("sat_writing_avg_score")
    val writing: String,
)

data class HighSchoolDbn(
    @SerializedName("dbn")
    val id: String
)